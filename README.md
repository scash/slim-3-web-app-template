# General Information

This is intended to act as a starting point for any [Slim 3](http://www.slimframework.com/) web application. To get going, simply modify the application settings (via settings.php) and you're ready to begin coding project specifics.

### Requirements

PHP 5.6.x, [Composer](https://getcomposer.org/) (PHP Dependency Manager), and Apache must all be installed and functioning properly.

### Set Up Instructions

Create a virtual host entry within Apache pointing to the base directory, as direct access via localhost occasionally causes routing issues within Slim. Additionally, if not in production, create an environment variable ("APPLICATION_ENV") set to either ("development" or "staging"). For example, in Apache,

```sh
SetEnv APPLICATION_ENV development
```

### Installation

Install the codebase within a location accessible from the web server and then execute the following command,

```sh
composer install
```