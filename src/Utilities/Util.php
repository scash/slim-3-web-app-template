<?php

namespace Utilities;

/**
 * Utility methods
 *
 * @author Santos Cash
 */
class Util {

	/**
	 * Prepare an array of parameters for binding to a PDO::Statement
	 *
	 * @param	array	$arInput	An array of elements and values for binding
	 * @return	array	An array ready for binding
	 */
	public static function readyParams($arInput) {
		$arResult = array();

		foreach($arInput as $key => $value) {
			if($value !== FALSE) {
				$arResult[":" . $key] = $value;
			}
		}

		return $arResult;
	}

	/**
	 * Determine if the value of a variable represents an integer
	 *
	 * @param	mixed	$input	Input variable
	 * @return	bool	Whether the passed value can be cast to an int
	 */
	public static function isInteger($input) {
		return(ctype_digit(strval($input)));
	}

	/**
	 * Add months to a given date without any "rollover" effect
	 *
	 * @param	DateTime	$theStartDateTime	The DateTime representing the date to add time to
	 * @param	int	$count	Number of months to add to the DateTime value
	 * @return	DateTime	A DateTime representing the new date
	 */
	public static function addMonth(\DateTime $theStartDateTime, $count) {
		$theEndDateTime = clone $theStartDateTime;

		if($count > 0) {
			$theEndDateTime->modify("+$count month");

			while((($theStartDateTime->format("n") + $count) % 12) != ($theEndDateTime->format("n") % 12)) {
				$theEndDateTime->modify("-1 day");
			}
		}

		return $theEndDateTime;
	}

	/**
	 * Log and alert client to any errors encountered during application execution
	 *
	 * @param	Monolog\Logger	$logger	Object used to log application messages
	 * @param	string	$errorMsg	Error message
	 * @param	int	$statusCode	HTTP status code to send to the client
	 * @param	string	$specialCode	A code indicating specific application actions
	 * @return	Slim\Http\Response	A JSON response containing error information
	 */
	public static function handleError($logger, $errorMsg, $statusCode = 403, $specialCode = NULL) {

		// Log error
		$logger->error("Status: " . $statusCode . " -- " . $errorMsg);

		if(!is_null($specialCode)) {
			$error = new stdClass();
			$error->code = $specialCode;
			$error->message = $errorMsg;
		} else {
			$error = $errorMsg;
		}

		// Craft client response
		$response = new SlimResponse(array("error" => $error));
		$response->setStatusCode($statusCode);
		$response->setHeader("X-Status-Reason", $errorMsg);

		return $response->getResponse();
	}

}

?>