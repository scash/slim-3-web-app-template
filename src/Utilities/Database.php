<?php

namespace Utilities;

use PDO;

/**
 * A database connection class
 *
 * @author Santos Cash
 */
class Database {

	/**
	 * The database driver to base the connection on.
	 *
	 * @var	string
	 */
	private $driver;

	/**
	 * Constructor that sets the database driver to rely on.
	 *
	 * @param	string	$strDriver	Driver name
	 * @throws	Exception	When an invalid parameter is received
	 */
	public function __construct($strDriver) {

		// Ensure driver is a string type and at least one character in length
		if(is_string($strDriver) && strlen(trim($strDriver)) > 0) {
			$this->driver = strtolower(trim($strDriver));
		} else {
			throw new Exception(__METHOD__ . " >> invalid parameter value.");
		}
	}

	/**
	 * Establish a connection to a database.
	 *
	 * @param	array	$settings	Database connection settings
	 * @throws	PDOException	When a connection cannot be established
	 * @return	PDO	A connection to the database
	 */
	public function generateConn(array $settings) {

		switch($this->driver) {
			case "mysql":
				return $this->getMySqlConn($settings);
			case "pgsql":
				return $this->getPostgreSqlConn($settings);
		}
	}

	/**
	 * Establish a connection to a MySQL database.
	 *
	 * @param	array	$settings	Database connection settings
	 * @throws	PDOException	When a connection cannot be established
	 * @return	PDO	A connection to the database
	 */
	private function getMySqlConn(array $settings) {
		$strDSN = sprintf("mysql:hostname=%s;dbname=%s;port=%s;charset=utf8mb4", $settings["host"], $settings["dbname"], $settings["port"]);

		$dbConn = new PDO($strDSN, $settings["user"], $settings["pass"],
					array(
						PDO::ATTR_EMULATE_PREPARES => FALSE,
						PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
						PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8mb4'"
					)
		);

		return $dbConn;
	}

	/**
	 * Establish a connection to a PostgreSQL database.
	 *
	 * @param	array	$settings	Database connection settings
	 * @throws	PDOException	When a connection cannot be established
	 * @return	PDO	A connection to the database
	 */
	private function getPostgreSqlConn(array $settings) {
		$strDSN = sprintf("pgsql:hostname=%s;dbname=%s;port=%s", $settings["host"], $settings["dbname"], $settings["port"]);

		$dbConn = new PDO($strDSN, $settings["user"], $settings["pass"],
					array(
						PDO::ATTR_EMULATE_PREPARES => FALSE,
						PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
					)
		);

		// Set character set
		$dbConn->query("SET NAMES 'UTF8'");

		return $dbConn;
	}

}

?>