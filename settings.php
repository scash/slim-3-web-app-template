<?php

$dbSettings = array();				// Database settings
$allSettings = array();				// Aggregated settings
$monologSettings = array();			// Log settings
$bolDisplayErrorDetails = FALSE;	// Level of error information to display

// Ensure log entry is Pacific time zone
//	NOTE: Alternatively, this can be set within php.ini (date.timezone)
date_default_timezone_set("America/Los_Angeles");

// Retrieve application environment
//	NOTE: If application environment isn't set, then it's assumed we're running in production
$appMode = ((getenv("APPLICATION_ENV") === FALSE) ? "production" : strtolower(getenv("APPLICATION_ENV")));

switch($appMode) {
	case "development":
		$bolDisplayErrorDetails = TRUE;

		$dbSettings["host"] = "";				// MUST SET
		$dbSettings["user"] = "";				// MUST SET
		$dbSettings["pass"] = "";				// MUST SET

		break;
	case "staging":
		$dbSettings["host"] = "";				// MUST SET
		$dbSettings["user"] = "";				// MUST SET
		$dbSettings["pass"] = "";				// MUST SET

		break;
	case "production":
		$dbSettings["host"] = "";				// MUST SET
		$dbSettings["user"] = "";				// MUST SET
		$dbSettings["pass"] = "";				// MUST SET
}

// Monolog settings
$monologSettings["name"] = "slim-app";
$monologSettings["path"] = "./logs/" . date("Y-m-d") . ".log";

// Additional database settings
$dbSettings["port"] = "";						// MUST SET
$dbSettings["dbname"] = "";						// MUST SET
$dbSettings["driver"] = "";						// MUST SET

// Aggregate settings
$allSettings["mode"] = $appMode;
$allSettings["db"] = $dbSettings;
$allSettings["logger"] = $monologSettings;
$allSettings["displayErrorDetails"] = $bolDisplayErrorDetails;

?>